package me.deftare.emcverify;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Deftware
 */
public class Main {

	public static String latestVersion;
	public static JsonObject mavenJson, minecraftManifset;
	public static JsonArray versionsArray;
	public static final File cacheDir = new File("cache/");
	private static ExecutorService runService;
	private static AtomicInteger completedInstances = new AtomicInteger(0), totalInstances = new AtomicInteger(0);
	public static String EMCVersion = "13.9.6";

	public static void main(String[] args) {
		if (!cacheDir.exists() && !cacheDir.mkdirs()) {
			System.err.println("Failed to create cache dir");
		}
		System.out.println("Downloading maven json...");
		try {
			mavenJson = new Gson().fromJson(Utils.fetch("https://maven.aristois.net/versions.json"), JsonObject.class);
			minecraftManifset = new Gson().fromJson(Utils.fetch("https://launchermeta.mojang.com/mc/game/version_manifest.json"), JsonObject.class);
			latestVersion = minecraftManifset.get("latest").getAsJsonObject().get("release").getAsString();
			versionsArray = mavenJson.get("versions").getAsJsonArray();
			runService = Executors.newFixedThreadPool(versionsArray.size() + 1);
			// First fetch latest version to compare with
			System.out.println("Fetching latest version to compare with for fabric and mcp...");
			CompletableFuture.supplyAsync(() -> {
				EMCJar jar = new EMCJar(latestVersion, String.format("EMC-F-v2:%s-%s", EMCVersion, latestVersion));
				jar.run();
				return jar;
			}, runService).thenAccept(jar -> {
				System.out.println(jar.getVersion() + " [Latest version | Fabric] completed with " + jar.getCountMethods() + " methods!");
				verifyVersions(true, jar);
			});
			// MCP
			CompletableFuture.supplyAsync(() -> {
				EMCJar jar = new EMCJar("1.13.2", String.format("EMC:%s-%s", EMCVersion, "1.13.2"));
				jar.run();
				return jar;
			}, runService).thenAccept(jar -> {
				System.out.println(jar.getVersion() + " [MCP] completed with " + jar.getCountMethods() + " methods!");
				verifyVersions(false, jar);
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void verifyVersions(boolean fabric, EMCJar compareTo) {
		for (JsonElement version : versionsArray) {
			JsonObject data = version.getAsJsonObject();
			String versionData = data.get("version").getAsString();
			if (versionData.split("\\.").length == 2) {
				versionData += ".0";
			}
			if (Integer.parseInt(versionData.replaceAll("\\.", "")) >= 1_14_0 && fabric || Integer.parseInt(versionData.replaceAll("\\.", "")) < 1_14_0 && !fabric) {
				System.out.println(String.format("Verifying EMC version %s...", data.get("version").getAsString()));
				totalInstances.incrementAndGet();
				CompletableFuture.supplyAsync(() -> {
					EMCJar jar = new EMCJar(data.get("version").getAsString(), data.get("emc").getAsString().replace("latest", EMCVersion));
					jar.run();
					return jar;
				}, runService).thenAccept(jar -> {
					if (!jar.isEqualTo(compareTo, true)) {
						System.out.println("Anomaly detected in " + jar.getVersion() + "!");
					} else {
						System.out.println(jar.getVersion() + " Passed tests");
					}
					if (completedInstances.incrementAndGet() == totalInstances.get()) {
						runService.shutdown();
					}
				});
			}
		}
	}

}
