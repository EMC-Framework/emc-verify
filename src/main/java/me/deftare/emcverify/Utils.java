package me.deftare.emcverify;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.*;

/**
 * @author Deftware
 */
public class Utils {

	public static Callable<File> download(String uri, File file) {
		if (file.exists() && !file.delete()) {
			System.err.println("Failed to delete jar");
		}
		return () -> {
			URL url = new URL(uri);
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
			connection.setRequestMethod("GET");
			FileOutputStream out = new FileOutputStream(file);
			InputStream in = connection.getInputStream();
			int read;
			byte[] buffer = new byte[4096];
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			in.close();
			out.close();
			return file;
		};
	}

	public static String fetch(String url) throws Exception {
		URL url1 = new URL(url);
		Object connection = (url.startsWith("https://") ? (HttpsURLConnection) url1.openConnection()
				: (HttpURLConnection) url1.openConnection());
		((URLConnection) connection).setConnectTimeout(8 * 1000);
		((URLConnection) connection).setRequestProperty("User-Agent", "EMC Installer");
		((HttpURLConnection) connection).setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(((URLConnection) connection).getInputStream()));
		StringBuilder result = new StringBuilder();
		String text;
		while ((text = in.readLine()) != null) {
			result.append(text);
		}
		in.close();
		return result.toString();
	}

	public static String getSha1(File file) throws IOException {
		String sha1 = null;
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e1) {
			throw new IOException("Impossible to get SHA-1 digester", e1);
		}
		try (InputStream input = new FileInputStream(file);
		     DigestInputStream digestStream = new DigestInputStream(input, digest)) {
			while (digestStream.read() != -1) { }
			MessageDigest msgDigest = digestStream.getMessageDigest();
			sha1 = printHexBinary(msgDigest.digest());
		}
		return sha1;
	}

	private static final char[] hexCode = "0123456789ABCDEF".toCharArray();

	public static String printHexBinary(byte[] data) {
		StringBuilder r = new StringBuilder(data.length * 2);
		for (byte b : data) {
			r.append(hexCode[(b >> 4) & 0xF]);
			r.append(hexCode[(b & 0xF)]);
		}
		return r.toString();
	}

	public static String getMavenUrl(String name, String url) {
		String[] data = name.split(":");
		String type = data.length > 3 ? data[3] : "";
		return String.format("%s%s/%s/%s/%s-%s%s.jar", url, data[0].replaceAll("\\.", "/"), data[1], data[2], data[1], data[2], type);
	}

}
