package me.deftare.emcverify;

import lombok.Getter;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author Deftware
 */
public class EMCJar implements Runnable {

	private URLClassLoader classLoader;
	private @Getter boolean matching = true;
	private @Getter String version, mavenVersion;
	private @Getter File file;
	private @Getter JarFile jarFile;
	private DependencyManager dependencyManager;
	private HashMap<String, Class<?>> classes;
	private HashMap<String, HashMap<String, Method>> methods;

	public EMCJar(String version, String mavenVersion) {
		this.version = version;
		this.mavenVersion = mavenVersion;
		this.file = new File(String.format("%s/EMC-%s.jar", Main.cacheDir.getAbsolutePath(), version));
		this.dependencyManager = new DependencyManager(version);
	}

	@Override
	public void run() {
		try {
			if (!verifyChecksum()) {
				download().call();
			}
			// Preload the methods
			getMethods();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Checks if this jar is equal to a given jar (the latest jar)
	 */
	public boolean isEqualTo(EMCJar jar, boolean print) {
		boolean noAnomalies = true, classOrMethodError = false;
		int methodIgnoreCount = 0;
		for (String clazz : jar.getMethods().keySet()) {
			// Make sure class names match
			if (!getMethods().containsKey(clazz)) {
				if (print) {
					System.out.println(String.format("[%s] Class anomaly, class %s not found", version, clazz));
				}
				classOrMethodError = true;
				noAnomalies = false;
			}
			// Make sure method names match
			for (String method : jar.getMethods().get(clazz).keySet()) {
				if (!getMethods().get(clazz).containsKey(method)) {
					if (!isMinecraftFunction(method)) {
						if (print) {
							System.out.println(String.format("[%s] Method anomaly, method %s not found in class %s", version, method, clazz));
						}
						noAnomalies = false;
						classOrMethodError = true;
					} else {
						methodIgnoreCount++;
					}
				}
			}
		}
		// Make sure the amount of methods are equal first
		if (getCountMethods() != jar.getCountMethods() && classOrMethodError) {
			if (print) {
				System.out.println(String.format("[%s] Count anomaly, got %s, but should be %s. Ignored %s methods", version, getCountMethods(), jar.getCountMethods(), methodIgnoreCount));
			}
			noAnomalies = false;
		}
		return noAnomalies;
	}

	/**
	 * Returns functions which are Minecraft internal and should not be compared between versions
	 */
	public boolean isMinecraftFunction(String method) {
		method = method.toLowerCase();
		// method_ (Fabric)
		if (method.startsWith("method_")) {
			return true;
		}
		// rendering
		if (method.contains("blit") || method.contains("fill") || method.contains("narration")) {
			return true;
		}
		// low count letter methods, common in MCP
		if (method.split("\\(")[0].length() <= 3) {
			return true;
		}
		// events
		if (method.toLowerCase().contains("typed") || method.toLowerCase().contains("released") || method.toLowerCase().contains("focused") || method.toLowerCase().contains("pressed") || method.toLowerCase().contains("dragged") || method.toLowerCase().contains("clicked") || method.toLowerCase().contains("scrolled")) {
			return true;
		}
		// Inherited functions in Minecraft classes
		if (method.contains("confirm") || method.contains("modifiers") || method.contains("action") || method.contains("raw")) {
			return true;
		}
		// Ignored methods
		if (method.contains("getEntityArgumentType".toLowerCase()) || method.contains("compareTo".toLowerCase()) || method.contains("clonePlayer".toLowerCase())) {
			return true;
		}
		return false;
	}

	/**
	 * Convert a native Minecraft dependant class to NativeClass
	 */
	public String convertNative(String input) {
		// We don't care about primitive values
		if (input.equalsIgnoreCase("byte") || input.equalsIgnoreCase("short") || input.equalsIgnoreCase("int") || input.equalsIgnoreCase("long") ||
				input.equalsIgnoreCase("float") || input.equalsIgnoreCase("double") || input.equalsIgnoreCase("char") || input.equalsIgnoreCase("boolean")) return input;
		// Default classes with no package
		if (!input.contains(".")) input = "NativeClass";
		// Minecraft libraries
		if (input.contains("datafixers")) input = "NativeClass";
		return input;
	}

	public int getCountMethods() {
		int methodsCount = 0;
		for (HashMap<String, Method> methods : methods.values()) {
			methodsCount += methods.size() + 1;
		}
		return methodsCount;
	}

	public HashMap<String, HashMap<String, Method>> getMethods() {
		if (methods == null) {
			methods = new HashMap<>();
			try {
				getClasses().forEach((name, clazz) -> {
					try {
						Arrays.stream(clazz.getMethods()).forEach(method -> {
							if (!methods.containsKey(clazz.getCanonicalName())) {
								methods.put(clazz.getCanonicalName(), new HashMap<>());
							}
							String methodKey = String.format("%s(%s)%s", method.getName(), parametersToString(method.getParameterTypes()), method.getParameterCount());
							if (methods.get(clazz.getCanonicalName()).containsKey(methodKey)) {
								// System.out.println("Duplicate method? > " + methodKey);
							}
							methods.get(clazz.getCanonicalName()).put(methodKey, method);
						});
					} catch (Throwable ex) {
						System.err.println("Skipping class " + clazz.getName() + " missing " + ex.getMessage());
					}
				});
			} catch (Exception ignored) { }
		}
		return methods;
	}

	public String parametersToString(Class<?>[] params) {
		AtomicReference<String> paramNames = new AtomicReference<>("");
		Arrays.stream(params).forEach(p -> paramNames.set(paramNames.get() + ", " + convertNative(p.getCanonicalName())));
		return !paramNames.get().isEmpty() ? paramNames.get().substring(", ".length()) : "";
	}

	public HashMap<String, Class<?>> getClasses() throws Exception {
		if (!file.exists()) throw new RuntimeException("Can not enumerate classes of non-existent jar!");
		if (classes == null) {
			classes = new HashMap<>();
			jarFile = new JarFile(file.getAbsolutePath());
			for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements(); ) {
				JarEntry entry = entries.nextElement();
				String file = entry.getName();
				if (file.endsWith(".class")) {
					String classname = file.replace('/', '.').substring(0, file.length() - 6);
					// We don't care about mixin classes or launch classes as they change in each version of EMC
					if (classname.startsWith("me.deftware.client.framework")) {
						try {
							classes.put(classname, getClassLoader().loadClass(classname));
						} catch (Throwable ignored) { }
					}
				}
			}
		}
		return classes;
	}

	@SuppressWarnings("ToArrayCallWithZeroLengthArrayArgument")
	public ClassLoader getClassLoader() throws Exception {
		if (!file.exists()) throw new RuntimeException("Can not create classloader to non-existent jar!");
		if (classLoader == null) {
			List<URL> dependencyURLs = new ArrayList<>(Collections.singletonList(file.toURI().toURL()));
			dependencyManager.getDependencies().call().forEach(file -> {
				try {
					dependencyURLs.add(file.toURI().toURL());
				} catch (MalformedURLException ignored) { }
			});
			classLoader = new URLClassLoader(dependencyURLs.toArray(new URL[dependencyURLs.size()]), null);
		}
		return classLoader;
	}

	public Callable<File> download() {
		return () -> {
			if (!verifyChecksum()) {
				Utils.download(getMavenURL(), file).call();
			}
			return file;
		};
	}

	public boolean verifyChecksum() throws Exception {
		if (!file.exists()) return false;
		return getLocalChecksum().equalsIgnoreCase(getRemoteChecksum());
	}

	public String getRemoteChecksum() throws Exception {
		return Utils.fetch(String.format("%s.sha1", getMavenURL()));
	}

	public String getLocalChecksum() throws Exception {
		return Utils.getSha1(file);
	}

	public String getMavenURL() {
		return Utils.getMavenUrl(String.format("me.deftware:%s", mavenVersion), "https://gitlab.com/EMC-Framework/maven/raw/master/");
	}

}
