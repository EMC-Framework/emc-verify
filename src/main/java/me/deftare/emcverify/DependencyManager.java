package me.deftare.emcverify;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.Getter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author Deftware
 */
public class DependencyManager {

	private @Getter String version, jsonUrl;
	private @Getter boolean fabric = false;
	private @Getter YarnDeobfuscator deobfuscator;

	public DependencyManager(String version) {
		this.version = version;
		Main.minecraftManifset.get("versions").getAsJsonArray().forEach(v -> {
			JsonObject data = v.getAsJsonObject();
			if (data.get("id").getAsString().equals(version)) {
				jsonUrl = data.get("url").getAsString();
			}
		});
		String versionData = version;
		if (versionData.split("\\.").length == 2) {
			versionData += ".0";
		}
		if (Integer.parseInt(versionData.replaceAll("\\.", "")) >= 1_14_0) {
			fabric = true;
			deobfuscator = new YarnDeobfuscator(version);
		}
	}

	public Callable<List<File>> getDependencies() {
		return () -> {
			List<File> dependencies = new ArrayList<>();
			// Version specific dependencies
			File client = downloadClient(new File(String.format("%s/Minecraft-%s.jar", Main.cacheDir, version))).call();
			if (fabric) {
				// De-obfuscate jar
				dependencies.add(deobfuscator.deobfuscate(client, new File(String.format("%s/Minecraft-%s-Intermediate.jar", Main.cacheDir, version))).call());
			} else {
				dependencies.add(client);
			}
			// Universal dependencies
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("com.mojang:brigadier:1.0.14", "https://libraries.minecraft.net/"), new File(String.format("%s/Brigadier.jar", Main.cacheDir))).call());
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("com.mojang:authlib:1.6.25", "https://libraries.minecraft.net/"), new File(String.format("%s/authlib.jar", Main.cacheDir))).call());
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("com.mojang:datafixerupper:2.0.24", "https://libraries.minecraft.net/"), new File(String.format("%s/datafixer.jar", Main.cacheDir))).call());
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("io.netty:netty-all:4.1.25.Final", "https://libraries.minecraft.net/"), new File(String.format("%s/netty.jar", Main.cacheDir))).call());
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("com.google.code.gson:gson:2.8.6", "https://repo1.maven.org/maven2/"), new File(String.format("%s/gson.jar", Main.cacheDir))).call());
			dependencies.add(downloadMavenDependency(Utils.getMavenUrl("com.google.guava:guava:21.0", "https://repo1.maven.org/maven2/"), new File(String.format("%s/guava.jar", Main.cacheDir))).call());
			return dependencies;
		};
	}

	public Callable<File> downloadMavenDependency(String url, File file) {
		return () -> {
			if (!file.exists()) {
				Utils.download(url, file).call();
			}
			return file;
		};
	}

	public Callable<File> downloadClient(File file) {
		return () -> {
			try {
				JsonObject versionData = new Gson().fromJson(Utils.fetch(jsonUrl), JsonObject.class);
				JsonObject clientData = versionData.get("downloads").getAsJsonObject().get("client").getAsJsonObject();
				if (!file.exists() || !Utils.getSha1(file).equalsIgnoreCase(clientData.get("sha1").getAsString())) {
					Utils.download(clientData.get("url").getAsString(), file).call();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return file;
		};
	}

}
