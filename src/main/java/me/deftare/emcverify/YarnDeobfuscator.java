package me.deftare.emcverify;

import lombok.Getter;
import net.fabricmc.tinyremapper.OutputConsumerPath;
import net.fabricmc.tinyremapper.TinyRemapper;
import net.fabricmc.tinyremapper.TinyUtils;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * Creates an intermediate jar file with fabric for Minecraft >= 1.14
 *
 * @author Deftware
 */
public class YarnDeobfuscator {

	private @Getter String version;

	public YarnDeobfuscator(String version) {
		this.version = version;
	}

	public Callable<File> deobfuscate(File minecraftJar, File outputJar) {
		return () -> {
			if (!outputJar.exists()) {
				File mappings = new File(String.format("cache/%s.tiny", version));
				Utils.download(String.format("https://github.com/FabricMC/intermediary/raw/master/mappings/%s.tiny", version), mappings).call();
				TinyRemapper remapper = TinyRemapper.newRemapper().withMappings(TinyUtils.createTinyMappingProvider(mappings.toPath(), "official", "intermediary")).renameInvalidLocals(true).rebuildSourceFilenames(true).build();
				OutputConsumerPath outputConsumer = new OutputConsumerPath(outputJar.toPath());
				remapper.readInputs(minecraftJar.toPath());
				remapper.apply(outputConsumer);
				outputConsumer.close();
				remapper.finish();
			}
			return outputJar;
		};
	}

}
